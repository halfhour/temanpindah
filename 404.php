<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package moverfriend
 */

get_header();
?>

	<main id="primary" class="my-20 py-20 text-center">

		<section>
			<header>
				<h1 class="entry-title"><?php esc_html_e( 'Opps! Halaman tidak ditemukan.', 'moverfriend' ); ?></h1>
			</header>

			<div class="entry-content">
				<p><?php esc_html_e( 'Sepertinya halaman yang kamu tuju tidak ditemukan. Mari kembali ke halaman awal.', 'moverfriend' ); ?></p>
				<a href="<?php echo home_url(); ?>" class="border-2 border-red-500 no-underline text-sm rounded py-3 px-8">Kembali</a>


					<!-- <?php
					get_search_form();

					the_widget( 'WP_Widget_Recent_Posts' );
					?> -->

					<!-- <div>
						<h2><?php esc_html_e( 'Most Used Categories', 'moverfriend' ); ?></h2>
						<ul>
							<?php
							wp_list_categories(
								array(
									'orderby'    => 'count',
									'order'      => 'DESC',
									'show_count' => 1,
									'title_li'   => '',
									'number'     => 10,
								)
							);
							?>
						</ul>
					</div> -->

					<!-- <?php
					/* translators: %1$s: smiley */
					$moverfriend_archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'moverfriend' ), convert_smilies( ':)' ) ) . '</p>';
					the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$moverfriend_archive_content" );

					the_widget( 'WP_Widget_Tag_Cloud' );
					?> -->

			</div>
		</section>

	</main><!-- #main -->

<?php
get_footer();
