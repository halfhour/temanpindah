<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package moverfriend
 */

get_header();
?>

	<main id="primary" class="md:mx-10">

	<!-- <?php


		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1><?php single_post_title(); ?></h1>
				</header>
				<?php
			endif;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
	?> -->

		<!-- Main Section -->
		<div class="my-20 md:flex md:space-y-1 space-y-10 justify-between items-center">
			<div>
				<h1 class="md:text-5xl text-4xl font-bold">Jasa Pindahan Rumah, Kantor dan Apartement</h1>
				<p class="mb-10 mt-3 text-gray-700 font-light">Jasa Angkut Berkualitas Untuk Semua Kebutuhan Pindahan Anda</p>
				<a href="#" class="bg-red-500 text-white rounded px-8 py-3">Pesan Sekarang</a>
			</div>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/truck-movers.png" width="400px" alt="Moving Goods">
		</div>

		<!-- About Section -->
		<div class="my-20 py-20 animated fadeInUp duration3 eds-on-scroll  grid md:grid-cols-2 items-center" id="tentang-kami">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/deliveries.svg" alt="Package Moving Goods" width="400px">
			<div class="md:order-last order-first">
				<h4 class="text-4xl font-bold">Jasa pindahan terbaik Anda</h4>
				<p class="font-light text-gray-600 my-8">
					Kami bukan hanya sekedar jasa pindahan biasa, juga bukan hanya mengangkat dan menurunkan barang-barang pindahan rumah Anda saja. Melainkan, kami yang memastikan barang-barang pindahan Anda aman dari kerusakan dan kehilangan.
					<br><br>
					Kami yang memastikan barang pindahan Anda tertata rapih di rumah baru.
				</p>
				<a href="<?php echo home_url(); ?>/tentang-kami" class="bg-red-500 text-sm text-white rounded py-3 px-8">Selengkapnya</a>
			</div>
		</div>

		<!-- Service Section -->
		<div class="my-20 py-20" id="service">
			<h4 class="text-4xl text-center font-bold animated fadeInUp duration3 eds-on-scroll">Jasa Pindahan yang Kami Tawarkan</h4>

			<div class="md:grid grid-cols-4 gap-2 mt-10">
				<div class="bg-white hover:border-red-500 border rounded-md p-5 animated fadeInUp duration3 eds-on-scroll">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/house.svg" class="w-20 h-20">
					<div class="text-md font-bold mt-8">Pindah Rumah</div>
					<p class="font-light text-gray-600 text-sm">Proses pindah rumah dari lokasi tempat Anda tinggal saat ini ke rumah baru memerlukan usaha yang tidak sedikit.</p>
				</div>

				<div class="bg-white hover:border-red-500 border rounded-md p-5 animated fadeInUp duration3 eds-on-scroll">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/apartement.svg" class="w-20 h-20">
					<div class="text-md font-bold mt-8">Pindah Apartement</div>
					<p class="font-light text-gray-600 text-sm">Hunian vertikal modern kini menjadi tren karena harga rumah yang melonjak tinggi.</p>
				</div>

				<div class="bg-white hover:border-red-500 border rounded-md p-5 animated fadeInUp duration3 eds-on-scroll">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/office.svg" class="w-20 h-20">
					<div class="text-md font-bold mt-8">Pindah Kantor</div>
					<p class="font-light text-gray-600 text-sm">Tidak ada yang sederhana dari merelokasi satu tim pegawai berikut perlengkapan kantor ke sebuah tempat baru.</p>
				</div>

				<div class="bg-white hover:border-red-500 border rounded-md p-5 animated fadeInUp duration3 eds-on-scroll">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/piano.svg" class="w-20 h-20">
					<div class="text-md font-bold mt-8">Pindah Piano</div>
					<p class="font-light text-gray-600 text-sm">Pindahan piano membutuhkan tekhnik dan alat khusus serta proses packing yang harus benar - benar aman dan rapih.</p>
				</div>

				<div class="bg-white hover:border-red-500 border rounded-md p-5 animated fadeInUp duration3 eds-on-scroll">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/atm.svg" class="w-20 h-20">
					<div class="text-md font-bold mt-8">Pindah ATM</div>
					<p class="font-light text-gray-600 text-sm">Mengingat fungsinya yang sangat vital, anda harus sangat berhati-hati ketika hendak melakukan pemindahan atm.</p>
				</div>
				
				<div class="bg-white hover:border-red-500 border rounded-md p-5 animated fadeInUp duration3 eds-on-scroll">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/cargo.svg" class="w-20 h-20">
					<div class="text-md font-bold mt-8">Jasa Kargo</div>
					<p class="font-light text-gray-600 text-sm">Cargo adalah barang yang dikirim oleh seseorang ke beberapa tempat tujuan melalui jalur darat, jalur udara, dan jalur laut.</p>
				</div>

				<div class="bg-white hover:border-red-500 border rounded-md p-5 animated fadeInUp duration3 eds-on-scroll">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logistics.svg" class="w-20 h-20">
					<div class="text-md font-bold mt-8">Jasa Logistik</div>
					<p class="font-light text-gray-600 text-sm">Jasa logistik adalah perusahaan yang menyediakan jasa kirim barang, gudang, manajemen, hingga transportasi.</p>
				</div>
			</div>
		</div>

		<!-- Why Section 3 -->
		<div class="my-20 py-20 grid md:grid-cols-2">
			<div class="max-w-md animated fadeInUp duration2 eds-on-scroll">
				<h4 class="text-4xl font-bold">Kenapa harus kami?</h4>
				<p class="text-md font-light text-gray-600 my-5">
					Kami mengerjakan setiap pelayanan yang kami berikan dengan hati-hati dan didukung oleh pekerja yang jujur, berkualitas, bertanggung jawab dan berpengalaman di bidangnya.
					<br><br>
					Area layanan pindahan kami :
				</p>

				<div>
					<ul class="space-y-5 text-lg font-light">
						<li class="flex items-center gap-5"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/check.svg" class="w-8">Pindahan JABODETABEK<li>
						<li class="flex items-center gap-5"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/check.svg" class="w-8">Pindahan Keluar Kota<li>
						<li class="flex items-center gap-5"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/check.svg" class="w-8">Pindahan Antar Pulau<li>
						<li class="flex items-center gap-5"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/check.svg" class="w-8">Pindahan Keluar Negeri<li>
					</ul>
				</div>
			</div>

			<div class="md:grid md:grid-cols-2 grid-flow-col gap-5">
				<div class="col-start-1 mt-10">
					<div class="bg-white border rounded-md p-5 mb-5 animated fadeInUp duration2 eds-on-scroll">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/briefcase.svg" class="bg-red-100 flex justify-center items-center rounded-full p-5 h-20 w-20">
						<h5 class="text-md font-bold mt-5 mb-1">Berpengalaman</h5>
						<p class="font-light text-gray-600 text-sm">Spesialis pindahan rumah membuat pekerjaan kami cepat & selesai</p>
					</div>
					<div class="bg-white border rounded-md p-5 mb-5 animated fadeInUp duration2 eds-on-scroll">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/cleanliness.svg" class="bg-red-100 flex justify-center items-center rounded-full p-5 h-20 w-20">
						<h5 class="text-md font-bold mt-5 mb-1">Kebersihan & Kerapihan</h5>
						<p class="font-light text-gray-600 text-sm">Bisa langsung tidur setelah proses transfer selesai</p>
					</div>
				</div>
				<div class="col-start-2">
					<div class="bg-white border rounded-md p-5 mb-5 animated fadeInUp duration2 eds-on-scroll">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/user.svg" class="bg-red-100 flex justify-center items-center rounded-full p-5 h-20 w-20">
						<h5 class="text-md font-bold mt-5 mb-1">Kru Professional</h5>
						<p class="font-light text-gray-600 text-sm">Tim packing dan dan crew angkat barang yang sudah terlatih</p>
					</div>
					<div class="bg-white border rounded-md p-5 mb-5 animated fadeInUp duration2 eds-on-scroll">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/lock.svg" class="bg-red-100 flex justify-center items-center rounded-full p-5 h-20 w-20">
						<h5 class="text-md font-bold mt-5 mb-1">Keamanan Barang</h5>
						<p class="font-light text-gray-600 text-sm">Jaminan resiko akan barang rusak, baret, dan penyok</p>
					</div>
				</div>
			</div>
		</div>

		<!-- How Section -->
		<div class="my-20 py-20">
			<div class="text-4xl font-bold md:text-center mb-12 animated fadeInUp duration2 eds-on-scroll">Bagaimana Cara Kami Membantu Proses Pindahan Anda?</div>

			<div class="md:grid md:grid-cols-2 grid-flow-col">
				<div class="border md:flex items-center rounded-md py-5 px-6 m-2 animated fadeInUp duration2 eds-on-scroll">
					<h1 class="text-6xl font-bold mr-6">1</h1>
					<div>
						<h5 class="text-2xl font-bold mb-2">Packing Barang Pindahan Rumah</h5>
						<span class="text-sm font-light">Sudah menjadi tanggung jawab kami untuk menjaga barang-barang penting Anda dari kerusakan & kotor seperti, spring bed, sofa, kulkas, mesin cuci dan barang-barang penting lain-nya. (Kami memberikan jaminan keamanan barang terhindar dari resiko rusak, pecah, hancur, dll)</span>
					</div>
				</div>
				<div class="border md:flex items-center rounded-md py-5 px-6 m-2 animated fadeInUp duration2 eds-on-scroll">
					<h1 class="text-6xl font-bold mr-6">2</h1>
					<div>
						<h5 class="text-2xl font-bold mb-2">Loading & Unloading</h5>
						<span class="text-sm font-light">Ditangani oleh crew angkat barang yang sudah terlatih dan sudah memiliki teknik mengangkat dan menurunkan barang yang efektif, serta didukung oleh peralatan penunjang yang menjadikan-nya lebih profesional.</span>
					</div>
				</div>
			</div>

			<div class="md:grid md:grid-cols-2 grid-flow-col">
				<div class="border md:flex items-center rounded-md py-5 px-6 m-2 animated fadeInUp duration2 eds-on-scroll">
					<h1 class="text-6xl font-bold mr-6">3</h1>
					<div>
						<h5 class="text-2xl font-bold mb-2">Unpacking</h5>
						<span class="text-sm font-light">Sesampainya dilokasi tujuan, kami membuka kembali packingan barang-barang pindahan yang ingin segera digunakan oleh Anda. (Lelah membuka packingan barang pindahan? Tidak perlu khawatir, kami yang membukanya kembali)</span>
					</div>
				</div>
				<div class="border md:flex items-center rounded-md py-5 px-6 m-2 animated fadeInUp duration2 eds-on-scroll">
					<h1 class="text-6xl font-bold mr-6">4</h1>
					<div>
						<h5 class="text-2xl font-bold mb-2">Re-posisi Barang Pindahan Rumah</h5>
						<span class="text-sm font-light">Anda sudah tidak perlu khawatir lagi tentang bagaimana cara mengangkat barang-barang Anda agar dapat berada di posisi yang sesuai dengan layout yang telah Anda siapkan. Menaruh lemari di lantai 2?, menaruh kulkas di pojok ruangan?menaruh mesin cuci di bagian dapur dan barang-barang besar lainnya?</span>
					</div>
				</div>
			</div>
		</div>

		<!-- Gallery -->
		<div class="my-20 py-20 text-center" id="galeri">
			<div class="text-4xl font-bold mb-12 animated fadeInUp duration2 eds-on-scroll">Galeri Kami</div>
			
			<div class="md:grid grid-cols-3 gap-2 mb-10">
				<img src="https://temanpindah.com/wp-content/uploads/2021/10/p6-min.webp" alt="Angkut Barang - temanpindah.com" class="w-full h-64 object-cover rounded-sm animated fadeInUp duration3 eds-on-scroll">

				<img src="https://temanpindah.com/wp-content/uploads/2021/10/p7-min.webp" alt="Gotong Barang - temanpindah.com" class="w-full h-64 object-cover rounded-sm animated fadeInUp duration3 eds-on-scroll">

				<img src="https://temanpindah.com/wp-content/uploads/2021/10/p9-min.webp" alt="Box Barang - temanpindah.com" class="w-full h-64 object-cover rounded-sm animated fadeInUp duration3 eds-on-scroll">

				<img src="https://temanpindah.com/wp-content/uploads/2021/10/20210925_114906-min-scaled.webp" alt="Box Barang - temanpindah.com" class="w-full h-64 object-cover rounded-sm animated fadeInUp duration3 eds-on-scroll">

				<img src="https://temanpindah.com/wp-content/uploads/2021/10/p10-min.webp" alt="Box Barang - temanpindah.com" class="w-full h-64 object-cover rounded-sm animated fadeInUp duration3 eds-on-scroll">

				<img src="https://temanpindah.com/wp-content/uploads/2021/10/p4-min.webp" alt="Barang Pindahan - temanpindah.com" class="w-full h-64 object-cover rounded-sm animated fadeInUp duration3 eds-on-scroll">
			</div>

			<a href="<?php echo home_url(); ?>/galeri" class="bg-red-500 text-sm text-white rounded mt-10 py-3 px-8 animated fadeInUp duration2 eds-on-scroll">Lihat Lainnya</a>
		</div>

		<!-- WhatsApp -->
		<div class="my-20 py-20 animated fadeInUp duration3 eds-on-scroll " id="kontak-kami">
			<h4 class="text-4xl font-bold">Hubungi Kami untuk Pricelistnya</h4>
			<p class="text-gray-600 font-light mt-2 mb-8">Untuk menentukan harga jasa renovasi, kami harus melakukan survey terlebih dahulu untuk mengetahui secara pasti apa saja yang ingin Anda renovasi. Segera ajukan jadwal survey melalui klik tombol di bawah.</p>
			<div class="space-x-2">
				<a href="https://api.whatsapp.com/send?phone=6285977880422&text=Halo%20Admin,Saya%20Mau%20Menanyakan%20Jasa%20Renovasi" class="bg-white border-2 border-green-500 text-green-500 px-8 py-3 rounded font-medium">WhatsApp 1</a>
				<a href="https://api.whatsapp.com/send?phone=6281284858462&text=Halo%20Admin,Saya%20Mau%20Menanyakan%20Jasa%20Renovasi" class="bg-white border-2 border-green-500 text-green-500 px-8 py-3 rounded font-medium">WhatsApp 2</a>
			</div>
		</div>

	</main>

<?php
// get_sidebar();
get_footer();
