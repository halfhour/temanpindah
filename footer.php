<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package moverfriend
 */

?>

	<div class="absolute left-0 w-full pt-20" style="background-color: #010023;">
		<div class="max-w-6xl mx-auto">
			<div class="md:mx-10 mx-5 md:space-y-0 space-y-10 md:grid grid-cols-4 text-white mb-20">
				<div>
					<div class="text-3xl font-poppins">
						<span class="text-red-500">teman</span>pindah
					</div>
					<p class="font-light text-sm mt-2">Jasa Pindahan Rumah, Kantor dan Apartement</p>
				</div>
				<div class="mx-auto">
					<div class="font-bold">Company</div>
					<ul class="font-light text-sm space-y-2 mt-3">
						<li><a href="#">About Us</a></li>
						<li><a href="#">Service</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>
				<div class="mx-auto">
					<div class="font-bold">Help</div>
					<ul class="font-light text-sm space-y-2 mt-3">
						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Terms & Condition</a></li>
						<li><a href="#">Credits</a></li>
					</ul>
				</div>
				<div class="mx-auto">
					<div class="font-bold">Get in Touch</div>
					<ul class="font-light text-sm space-y-2 mt-3">
						<li><a href="#">+62 85977880422 (Syamsul H)</a></li>
						<li><a href="#">+62 81284858462 (Vera S)</a></li>
						<li><a href="#">info@temanpindah.com</a></li>
					</ul>
				</div>
			</div>
			<div class="text-gray-500 text-center text-sm font-light py-5">Hak Cipta @2021 <span class="text-gray-400">temanpindah</span> | Didukung oleh berkahmoverjasapindahan</div>
		</div>
	</div>

	<!-- <footer id="colophon">
		<div>
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'moverfriend' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by azrielfatur', 'moverfriend' ) );
				?>
			</a>
			<span> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'moverfriend' ), 'moverfriend', '<a href="http://underscores.me/">Underscores.me</a>' );
				?>
		</div>
	</footer>#colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script>
	const setup = () => {
		return {
			isSidebarOpen: false,
		}
	}
</script>
<script defer src="https://unpkg.com/alpinejs@3.4.1/dist/cdn.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
</body>
</html>
