<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package moverfriend
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

	<style>
		.font-poppins {
            font-family: 'Poppins', sans-serif;
        }

		.font-ubuntu {
            font-family: 'Ubuntu', sans-serif;
        }
	</style>
	<?php wp_head(); ?>
</head>
<body <?php body_class('max-w-6xl mx-5 md:mx-auto font-ubuntu'); ?> x-data="setup()">
<?php wp_body_open(); ?>
<div id="page">

	<header id="masthead" class="flex justify-between items-end my-10">
		<div class="text-lg font-poppins">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="" rel="home">
					<span class="text-red-500">teman</span>pindah
				</a></h1>
				<?php
			else :
				?>
				<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="" rel="home">
					<span class="text-red-500">teman</span>pindah
				</a></p>
				<?php
			endif;
			?>
		</div>

		<!-- Only Mobile -->

		<nav id="site-navigation" class="md:block hidden">
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
					'menu_class'	 => 'flex items-center text-sm font-light gap-14'
				)
			);
			?>
		</nav><!-- #site-navigation -->
			<!-- Only Mobile -->
		<button class="sm:hidden pb-1" @click="isSidebarOpen = true"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/menu.svg" width="24px"></button>
        <div x-transition:enter="transform transition-transform duration-300"
            x-transition:enter-start="translate-x-full" x-transition:enter-end="-translate-x-0"
            x-transition:leave="transform transition-transform duration-300" x-transition:leave-start="-translate-x-0"
            x-transition:leave-end="translate-x-full" class="bg-white shadow-xl fixed top-0 right-0 w-3/5 h-screen z-10"
            x-show="isSidebarOpen">

			<button @click="isSidebarOpen = false" class="m-5"><img src="<?php echo get_template_directory_uri(); ?>/assets/close.svg" width="32px"></button>
			
			<div class="mx-5">
				<span class="text-gray-400 font-medium">MENUS</span>
			</div>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
					'menu_class'	 => 'm-5 text-gray-900 font-light space-y-3'
				)
			);
			?>
		</div>
	</header><!-- #masthead -->
